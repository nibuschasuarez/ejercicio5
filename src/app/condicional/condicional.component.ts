import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {

  NombreCompleto = "Daniela Soliz Languidey"
  Direccion = "Av. 24 de septiembre"

  mostrar = true;

  constructor() { }

  ngOnInit(): void {
  }

}
